package thesis.remote.project.server

import scala.concurrent.duration._
import akka.actor._
import akka.actor.FSM._
import thesis.remote.project.common.Messages._

//__________MESSAGES____________
sealed trait Message
final case class Greet(dude: String) extends Message
object Request extends Message
//final case class MLRequest(algorithm: String) extends Message
object ShutdownRequest extends Message


//___________STATES___________
sealed trait FrontendState
case object Available extends FrontendState
case object ProcessingRequest extends FrontendState
case object ShuttingDown extends FrontendState

//_______STATE_CONTAINER_______
final case class ActorRefRequest(inquirer: ActorRef)

class FrontendActor extends Actor with FSM[FrontendState,ActorRefRequest]{

  import context._

  //We must specify the state and the state container initialized as deadLetters (~NULL pointer)
  startWith(Available, ActorRefRequest(system.deadLetters))

  println("A: FrontendActor: UP and RUNNING")

  //Inside the when block we put everything that the actor must do in that state
  when(Available){

    //We need to write the behaviour of the FSM when an event occurrs
    //stateFunction argument is a PartialFunction[Event, State]
    case Event(Greet(name),_) =>
      println(s"A: Ave $name")
      stay
    case Event(Request,_)=>
      goto(ProcessingRequest) using ActorRefRequest(sender())
    case Event(ShutdownRequest, _) =>
      println("A: Shutting down")
      stop(Normal)
    //context.stop(self)
    //goto(ShuttingDown) using ActorRefRequest(system.deadLetters)
  }

  when(ProcessingRequest){
    case Event(MLRequest(algorithm, similarity, k),ActorRefRequest(inquirer)) =>
      println(s"Processing my request of algo $algorithm from sender $inquirer")
      goto(Available) using ActorRefRequest(system.deadLetters)
  }


}
/*
def receive = {
  case Greet(name) =>
    println(s"A: Ave $name")
  case ShutdownRequest =>
    println("A: Now I am killing myself, but only because you told me")
    context.stop(self)
  case _ =>
    println("A: I do not understand the msg")
}
*/