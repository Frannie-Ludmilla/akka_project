package thesis.remote.project.server

import java.io.File

import thesis.remote.project.server.ADT.NecessaryPaths

import sys.process._

object FlinkUtils{

  def	checkFlink(): Boolean = {
    //If a non zero value with which is returned...the program was NOT available on my $PATH..
    val whichResultStart = "which start-local.sh".!
    val whichResultStop = "which stop-local.sh".!
    val whichFlink = "which flink".!
    whichResultStart==0 && whichResultStop==0 && whichFlink==0
  }

  def firing(): Boolean = {
    println("Executing start-local.sh and start-webclient.sh")
    // cmd1 #&& cmd2 --> execute command cmd2 if cmd1 has nonzero return value
    //("start-local.sh" #&& "start-webclient.sh").! ==0
    ("start-local.sh").! ==0
  }

  def closing(): Boolean = {
    println("Executing stop-webclient.sh and stop-local.sh")
    //cmd1 ### cmd2 ---> execute cmd2 after cmd1 completion, no matter cmd1 return value
    //("stop-webclient.sh" ### "stop-local.sh").! ==0
    ("stop-local.sh").! ==0
  }

  def checkAvailability(alg: String, paths: NecessaryPaths) : Boolean = {
    val job= paths.home+"flink_knn-0.1.jar"
    println(job)
    val inputFile= new File(job)
    if(inputFile.exists()) true else false
  }

  // [-k num] [-m euclidean | gower] [-outputPath path_to_output] [-s (itemtoParse)]
  def launchingProgram(pathJob: String, k:Int, metric: String, outputPath: String, record: String, paths: NecessaryPaths): Boolean= {
    // If there are all Flink binaries....start them up!
    if (checkFlink()) FlinkUtils.firing() else return false
    //val FlinkIsUp : Boolean= if(FlinkIsPresent) FlinkUtils.firing() else false

    val setArg: String = pathJob match {
      case s if s matches "(?i)KNN_Thyroid" => "thyroid"
      case s if s matches "(?i)KNN_SUSY" => "susy"
    }

    val inputFolderPath :String = pathJob match {
      case s if s matches "(?i)KNN_Thyroid" => paths.pathThyroid
      case s if s matches "(?i)KNN_SUSY" => paths.pathSusy
    }
    //Run a test

    //My configuration settings like where to find the job to fire
    val job2execute= "flink_knn-0.1.jar"
    val job = paths.home+job2execute

    //Fire the job and store the result in a file
    //val output = Seq("ls", "-a", "-l", "/tmp").!!
    try{
      val execution= Seq("/bin/sh","-c",s"flink run $job --set $setArg --k $k --m $metric --outputPath $outputPath --in $inputFolderPath --s $record").!
      println(s"execution value: $execution")
      //println("waiting 0.5 secs before shutdown")
      //Thread.sleep(500)
    }catch{ case e: Exception => println(e)}
    finally {
      FlinkUtils.closing()
    }
    true
  }
}

object SparkUtils{

  def	checkSpark(): Boolean = {
    val whichResult = "which spark-submit".!
    //If a non zero value is returned...no spark-submit was available on my $PATH
    whichResult==0
  }

  def checkAvailability(alg: String, paths: NecessaryPaths): Boolean ={
    val job= alg match {
      case s if s matches "(?i)KNN_Thyroid" => paths.home+"sparkthyroid_2.11-1.0.jar"
      case s if s matches "(?i)KNN_SUSY" => paths.home+"sparksusy_2.11-1.0.jar"
    }
    val inputFile= new File(job)
    if(inputFile.exists()) true else false
  }

  def firing(): Boolean ={ true }

  def launchingProgram(pathJob: String, k:Int, metric: String, outputPath: String, record: String, paths:NecessaryPaths): Boolean = {
    val job= pathJob match {
      case s if s matches "(?i)KNN_Thyroid" => paths.home+"sparkthyroid_2.11-1.0.jar"
      case s if s matches "(?i)KNN_SUSY" =>  paths.home+"sparksusy_2.11-1.0.jar"
    }
    val inputFolderPath :String = pathJob match {
      case s if s matches "(?i)KNN_Thyroid" => paths.pathThyroid
      case s if s matches "(?i)KNN_SUSY" => paths.pathSusy
    }

    val execution= Seq("/bin/sh","-c",s"spark-submit $job -k $k -m $metric -outputPath $outputPath -s $record -in $inputFolderPath").!
    println(s"Executed program returned: $execution")
    execution==0
  }
}
