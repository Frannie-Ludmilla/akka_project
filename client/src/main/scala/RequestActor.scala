package thesis.remote.project.client

import java.io.PrintWriter
import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}

import akka.actor.{Actor, ActorPath, ActorRef, ActorSelection, Identify, PoisonPill, Props}
import akka.cluster.client.{ClusterClient, ClusterClientSettings}
import akka.cluster.protobuf.msg.ClusterMessages.NodeMetrics.Metric
import thesis.remote.project.common.Messages._

import scala.concurrent.duration._

object RequestActor {
  def props(algorithm: String, metric: String, k: Int, record: String, outputPath: String, receptionist: String) : Props = Props(classOf[RequestActor],algorithm,metric,k,record,outputPath,receptionist)

  case object Tick
  val phrase = "I need Help with this Task!"
}

/** RequestActor is the actor that sends its requests to its corresponding ClusterNode
  *  that will publish its request in the remote cluster.
  *
  * @constructor creates an actor to talk to the ClusterNode
  */
class RequestActor(algorithm: String, metric: String, k: Int, record: String, outputPath: String, receptionist: String) extends Actor{
  import RequestActor._
  import context.dispatcher

  def scheduler = context.system.scheduler

  /** Inside initialContacts I have to put the ActorPath of the /system/receptionist to which I want to connect */
  val contact: String="akka.tcp://CollaborativeCluster@"+receptionist+"/system/receptionist"

  val initialContacts =
    Set(ActorPath.fromString(contact))
    //ActorPath.fromString("akka.tcp://CollaborativeCluster@127.0.0.1:2552/system/receptionist"))

  /** I create an actor (ClusterClient) that will talk to the CollaborativeCluster forwarding the messages via the Receptionist */
  val clusterClientRef = context.system.actorOf(ClusterClient.props(
    ClusterClientSettings.create(context.system).withInitialContacts(initialContacts)), "myClusterClient")


  /**PreStart tells what the ActorSystem must do before firing it up the Actor*/
  override def preStart(): Unit ={
    scheduler.scheduleOnce(5.seconds, self, Tick)
    //scheduler.scheduleOnce(15.seconds, self, ShutdownRequest)
  }

  // override postRestart so we don't call preStart and schedule a new Tick
  override def postRestart(reason: Throwable): Unit = ()

  def receive = {
    case Tick =>
      //val msg = phrase
      val msg = ExternalRequest(self, MLRequest(algorithm,metric,k,record))
      println(s"I, ${self}, am sending $msg to $clusterClientRef")
      clusterClientRef ! ClusterClient.Send("/user/master", msg, localAffinity = true)
      //clusterClientRef ! ClusterClient.Send("/user/master", msg, localAffinity = true)
      //clusterClientRef ! ClusterClient.SendToAll("/user/master", msg)
      //mediator ! Publish(topic,ClusterNode.Publish(msg))
      //println(s"I am sending a publish req from me, $self to $mediator")
      //println(s"I am sending a publish req from me, $self to $masterProxy")
      //masterProxy ! ClusterNode.Message(self,msg)
    case msg : majordomoTask =>
      if(msg.res == true)
        println(s"I received a successfulTask from ${msg.clusterNode} via ${msg.majordomoNode}")
    case `failedTask` =>
      println("Cannot retrieve correct classification label")
      clusterClientRef ! PoisonPill
      context.system.terminate()
    case `askShutdown`=>
      clusterClientRef ! PoisonPill
      context.system.terminate()
    case msg: successfulTaskResult =>
      println(s"The classification label is ${msg.label}")
      //Print to file the label
      Some(new PrintWriter(outputPath)).foreach{p => p.write(msg.label); p.close}
      //It cannot reply directly to sender because the sender is deadLetters as it is outside the cluster!
      msg.majordomo ! `positiveAck`
      context.system.terminate()
    //self ! askShutdown
  }
}