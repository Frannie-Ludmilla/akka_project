package thesis.remote.project

import scala.concurrent.forkjoin.ThreadLocalRandom
import scala.concurrent.duration._
import akka.actor.{Actor, ActorPath, ActorRef, ActorSelection, Identify, Props}
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.{Publish, Send, SendToAll, Subscribe, Unsubscribe, UnsubscribeAck}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import thesis.remote.project.ClusterNode.Message
import akka.cluster.client.{ClusterClient, ClusterClientReceptionist, ClusterClientSettings}

import scala.concurrent.duration._
import akka.pattern.ask


object RequestActor {
  case object Tick
  val phrase = "I need Help with this Task!"
}

/** RequestActor is the actor that sends its requests to its corresponding ClusterNode
  *  that will publish its request in the remote cluster.
  *
  * @constructor creates an actor to talk to the ClusterNode
  */
class RequestActor extends Actor{
  import RequestActor._
  import context.dispatcher
  //val client= context.actorOf(ClusterNode.props(self.path.name), "client")
  val mediator = DistributedPubSub(context.system).mediator
  def scheduler = context.system.scheduler
  val topic = "MLTask"

  /*
  val masterProxy = context.actorOf(
    ClusterSingletonProxy.props(
      settings = ClusterSingletonProxySettings(context.system).withRole("backend"),
      singletonManagerPath = "/user/master"
    ),
    name = "masterProxy")*/

/*
  val initialContacts: Set[ActorSelection] = Set(
    context.system.actorSelection("akka.tcp://Cluster@127.0.0.1:2552/system/receptionist"),
    context.system.actorSelection("akka.tcp://Cluster@127.0.0.1:2551/system/receptionist")
  )*/

//Well written
  val initialContacts = Set(
  ActorPath.fromString("akka.tcp://CollaborativeCluster@127.0.0.1:2552/system/receptionist")
)

  val clusterClientRef = context.system.actorOf(ClusterClient.props(ClusterClientSettings.create(context.system).withInitialContacts(initialContacts)), "myclient")
  //  ActorPath.fromString("akka.tcp://Cluster@127.0.0.1:2552/system/receptionist"))
    //ActorPath.fromString("akka://Cluster/system/receptionist"))
    //val settings = ClusterClientSettings(context.system).withInitialContacts(initialContacts)


  //val receptionist = context.system.actorOf(ClusterClient.props( ClusterClientSettings(context.system).withInitialContacts(initialContacts)), "client")

  /**PreStart tells what the ActorSystem must do before firing it up the Actor*/
  override def preStart(): Unit ={
    scheduler.scheduleOnce(5.seconds, self, Tick)
    //scheduler.scheduleOnce(15.seconds, self, ShutdownRequest)
  }

  // override postRestart so we don't call preStart and schedule a new Tick
  override def postRestart(reason: Throwable): Unit = ()

  def receive = {
    case Tick =>
      val msg = phrase
      println(s"I, ${ActorRef.noSender}, am sending $msg to $clusterClientRef")
      //clusterClientRef ! ClusterClient.Send("user/master", msg, localAffinity = true)
      clusterClientRef ! ClusterClient.SendToAll("/user/master", msg)
      //mediator ! Publish(topic,ClusterNode.Publish(msg))
      //println(s"I am sending a publish req from me, $self to $mediator")
      //println(s"I am sending a publish req from me, $self to $masterProxy")
      //masterProxy ! ClusterNode.Message(self,msg)
    case `successfulTask` =>
      println(s"ReqActor: received successful task from $sender")
    case ShutdownRequest=>
      mediator ! ShutdownRequest
  }
}