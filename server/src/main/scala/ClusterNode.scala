package thesis.remote.project.server

import akka.actor._
import akka.serialization._
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.{Publish, Subscribe, Unsubscribe, UnsubscribeAck}
import akka.cluster.client.ClusterClientReceptionist
import akka.cluster.Cluster
import thesis.remote.project.common.Messages._
import thesis.remote.project.server.ADT.NecessaryPaths
import thesis.remote.project.server.ClusterMessage._
import thesis.remote.project.server.ADT.NecessaryPaths._

/** Factory Methods for ClusterNode instances. */
object ClusterNode{
  def props(name: String, home: String, susy: String, thyroid: String) : Props = Props(classOf[ClusterNode], name, home,susy,thyroid)
}


/** ClusterNode is the actor that acts as a Cluster Node, with a mediator to publish.
  * We will send messages from RequestActor to ClusterNode such that this last one can
  * then spread the Request for the ML task to all the other nodes in the Cluster.
  *
  * @constructor creates an actor inside a DistributedPublisher-Subscriber pattern in akka
  * @param name the actor name
  */
class ClusterNode(name: String, home: String, susy: String, thyroid: String) extends Actor{
  //Need to import context or become() won't work
  import context._

  val cluster = Cluster(context.system)

  val mediator = DistributedPubSub(context.system).mediator

  /**I have to specify the topic*/
  val topic = "MLTask"
  mediator ! Subscribe(topic,self)
  println(s"Actor ${self.path} joined the cluster")

  val paths: NecessaryPaths = new NecessaryPaths(home,susy,thyroid)

  def available : Receive= {

    case msg: ExternalRequest =>
      println(s" I received an external req from ${msg.from} " +
        s"to perform ${msg.mLRequest.algorithm} with ${msg.mLRequest.similarity} as default similarity")
      val majordomo= system.actorOf(Props(classOf[MajordomoActor],"majordomo", msg.from, paths.home))
      majordomo ! msg.mLRequest
      mediator ! Publish(topic, ClusterRequest(majordomo, self,msg.mLRequest))

    case msg: ClusterRequest =>
      //if(msg.clusterNode != self){
        println(s" A ML request arrived from ${msg.clusterNode} == ${sender()} and its supervisor is ${msg.majordomoNode}")
        println(s" I, ${name}, am creating a backend node")
        val backendNode: ActorRef= context.actorOf(Props(classOf[BackendNode], msg.majordomoNode, self, paths), "BN")
        backendNode ! msg.mLRequest
      //}

    case `NodeShutdown` =>
      sender() ! PoisonPill

    case `askShutdown` =>
      /**Unsubscribe from topic when asked to shutdown*/
      mediator ! Unsubscribe(topic,self)
      /** When I receive the ack for my unsubscription I can shut myself down*/
    case UnsubscribeAck =>
      context.stop(self)
    case x: String =>
      println(s"I received this: $x from ${sender()}")
        }

  def receive = available
}