package thesis.remote.project.client

import akka.actor.{ActorSystem, Props}
import com.typesafe.config.ConfigFactory


object MainApp extends App{

  def parseMetric(s: String): Option[String] = s match {
      case s if s matches "(?i)euclidean" => Some("euclidean")
      case s if s matches "(?i)gower" => Some("gower")
      case _ => None //If I do not recognize the metric, the value will be None
    }

  def parseAlgoritm(s: String): Option[String] = s match {
    case s if s matches "(?i)KNN_Thyroid" => Some("KNN_Thyroid")
    case s if s matches "(?i)KNN_SUSY" => Some("KNN_SUSY") //SuperSymmetric Particles
    case _ => None //If I do not recognize the algorithm, the value will be None
  }

  override def main(args: Array[String]): Unit = {
    //Get: 1 Algorithm 2 Similarity 3 K 4 Record 5 output

    val usage =
      """
    Usage: [-a algorithm KNN-Thyroid] [-m euclidean | gower] [-k num] [-outputPath path_to_output]  [-s record_to_parse_in_CSV]
      """
    //if (args.length == 0) println(usage)
    val arglist = args.toList
    type OptionMap = Map[Symbol, Any]

    def nextOption(map: OptionMap, list: List[String]): OptionMap = {
      def isSwitch(s: String) = (s(0) == '-')
      list match {
        case Nil => map
        case "-k" :: value :: tail =>
          nextOption(map ++ Map('k -> value.toInt), tail)
        case "-a" :: value :: tail =>
          nextOption(map ++ Map('algorithm -> value), tail)
        case "-m" :: value :: tail =>
          nextOption(map ++ Map('metric -> value), tail)
        case "-s" :: value :: tail =>
          nextOption(map ++ Map('record -> value), tail)
        case "-receptionist" :: value :: tail =>
          nextOption(map ++ Map('receptionist -> value), tail)
        case "-outputPath" :: value :: tail =>
          nextOption(map ++ Map('outputPath -> value), tail)
        case option :: tail => println("Unknown option " + option); Map()
      }
    }

    val options = nextOption(Map(), arglist)
    //println(options)

    val k_from_args: Option[Int] = options.get('k) match {
      case Some(s: Int) => Some(s)
      case _ => None
    }

    val algorithmfromArgs: Option[String] = options.get('algorithm) match {
      case Some(s: String) => parseAlgoritm(s)
      case _ => None //If I do not add the path from which I can take the files..stop
    }

    val outputfromArgs: Option[String] = options.get('outputPath) match {
      case Some(s: String) => Some(s)
      case _ => None //If I do not add the path from which I can take the files..stop
    }

    val metricfromArgs: Option[String] = options.get('metric) match {
      case Some(s: String) => parseMetric(s)
      case _ => None //If I do not add the path from which I can take the files..stop
    }

    val receptionistfromArgs: Option[String] = options.get('receptionist) match {
      case Some(s: String) => Some(s)
      case _ => None //If I do not add the path from which I can take the files..stop
    }

    val recordfromArgs: Option[String] = options.get('record) match {
      case Some(s: String) => Some(s.toString)
      case _ => None //If I do not add the path from which I can take the files..stop
    }

    def checkValues() : Boolean = {
      if (k_from_args.isDefined && algorithmfromArgs.isDefined && metricfromArgs.isDefined && recordfromArgs.isDefined && outputfromArgs.isDefined && receptionistfromArgs.isDefined)
        true
      else
        false
    }

    //If we have all the values, we can create the RequestActor
    if(checkValues()){
      val client_system = ActorSystem("MyClient", ConfigFactory.load("requestActor"))
      //The signature for RequestActor is (algorithm: String, metric: String, k: Int, record: String) e
      client_system.actorOf(Props(classOf[RequestActor], algorithmfromArgs.get,
        metricfromArgs.get, k_from_args.get, recordfromArgs.get, outputfromArgs.get, receptionistfromArgs.get), "Client")
    }
    else{
      //Print the usage and exit the program
      println(usage)
    }
  }
}
