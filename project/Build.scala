package thesis.remote.project

import sbt._
import sbt.Keys._


object Build extends sbt.Build{

  val commonSettings = Seq(
    organization := "com.uniroma1.pesci",
    version := "1.0",
    scalaVersion := "2.11.8",
    exportJars := true
  )

  lazy val akkaVersion = "2.4.3"

  val coreDependencies = Seq(
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-actor" % akkaVersion,
      "com.typesafe.akka" %% "akka-remote" % akkaVersion,
      "com.typesafe.akka" %% "akka-cluster" % akkaVersion,
      "com.typesafe.akka" %% "akka-cluster-tools" % akkaVersion,
      "com.typesafe.akka" %% "akka-persistence" % akkaVersion,
      "com.typesafe.akka" %% "akka-contrib" % akkaVersion,
      "com.typesafe.akka" %% "akka-stream" % akkaVersion,
      //"com.github.romix.akka" %% "akka-kryo-serialization" % "0.4.0",
      "com.typesafe" % "config" % "1.3.0"
  ))

//enablePlugins(JavaServerAppPackaging)
//fork in run := true

//mainClass in Compile := Some("thesis.remote.project.MainApp")
  scalacOptions in ThisBuild ++= Seq("-deprecation", "-unchecked", "-feature")
  //mainClass in (Compile, packageBin) in server := Some("package thesis.remote.project.server.MainApp")
  //mainClass in (Compile, packageBin) in client := Some("package thesis.remote.project.client.MainApp")

//See if required
//lazy val root = (project in file("."))

//mappings in Universal ++= {
  // optional example illustrating how to copy additional directory
  //directory("scripts") ++
  // copy configuration files to config directory
//  contentOf("src/main/resources").toMap.mapValues("config/" + _)
//}


// add ’config’ directory first in the classpath of the start script,
// an alternative is to set the config file locations via CLI parameters
// when starting the application
//scriptClasspath := Seq("../config/") ++ scriptClasspath.value

lazy val root = Project(id = "root",
  base = file(".") ,
  settings= commonSettings ++ coreDependencies
  )
  .aggregate(common, client, server) settings(
  name := "AkkaProject"
)


lazy val client = Project(id = "client",
  base = file("client"),
  settings = commonSettings ++ coreDependencies
) dependsOn(common)

lazy val server = Project(id = "server",
  base = file("server"),
  settings = commonSettings ++ coreDependencies
) dependsOn(common)

lazy val common = Project(id = "common",
  base = file("common"),
  settings = commonSettings ++ coreDependencies
)

}