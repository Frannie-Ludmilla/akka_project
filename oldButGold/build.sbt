import NativePackagerHelper._
import sbt._

lazy val commonSettings = Seq(
  organization := "thesis.remote.project",
  version := "1.0",
  scalaVersion := "2.11.7"
)

//enablePlugins(JavaServerAppPackaging)
//fork in run := true

//mainClass in Compile := Some("thesis.remote.project.MainApp")
scalacOptions in ThisBuild ++= Seq("-deprecation", "-unchecked", "-feature")
//See if required
//lazy val root = (project in file("."))

//mappings in Universal ++= {
  // optional example illustrating how to copy additional directory
  //directory("scripts") ++
  // copy configuration files to config directory
//  contentOf("src/main/resources").toMap.mapValues("config/" + _)
//}


// add ’config’ directory first in the classpath of the start script,
// an alternative is to set the config file locations via CLI parameters
// when starting the application
//scriptClasspath := Seq("../config/") ++ scriptClasspath.value

lazy val root = Project(id = "root",
  base = file("."))
  .aggregate(common, client, server).settings(
  name := "AkkaProject",

  libraryDependencies ++= {
    lazy val akkaVersion = "2.4.2"

    Seq(
      "com.typesafe.akka" %% "akka-actor" % akkaVersion,
      "com.typesafe.akka" %% "akka-remote" % akkaVersion,
      "com.typesafe.akka" %% "akka-cluster" % akkaVersion,
      "com.typesafe.akka" %% "akka-cluster-tools" % akkaVersion,
      "com.typesafe.akka" %% "akka-persistence" % akkaVersion,
      "com.typesafe.akka" %% "akka-contrib" % akkaVersion,
      //"com.typesafe.akka" %% "akka-stream-and-http-experimental" % "2.0.4",
      //"com.github.romix.akka" %% "akka-kryo-serialization" % "0.4.0",
      "com.typesafe" % "config" % "1.3.0"
    )
  }
)


lazy val client = Project(id = "client",
  base = file("client")) settings(commonSettings: _*) dependsOn(common)

lazy val server = Project(id = "server",
  base = file("server")) settings(commonSettings: _*) dependsOn(common)

lazy val common = Project(id = "common",
  base = file("common")) settings(commonSettings: _*)