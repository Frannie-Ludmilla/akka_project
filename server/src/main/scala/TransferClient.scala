package thesis.remote.project.server

import akka.actor.{Actor, ActorRef, Props}
import scala.concurrent._
import akka._
import akka.actor._
import akka.stream._
import akka.stream.scaladsl.{Tcp, _}
import akka.util._
import java.io.File
import scala.util.{Failure, Success}


object TransferClient{
  def props(backendNodeRef:ActorRef) : Props = Props(classOf[TransferClient], backendNodeRef)

  def client(system: ActorSystem, address: String, port: Int, transferClientRef: ActorRef, filepath:String): Unit = {
    import ClusterMessage._
    implicit val sys = system
    import system.dispatcher
    implicit val materializer = ActorMaterializer()

    val inputFile= new File(filepath)
    if(!inputFile.exists())
      transferClientRef ! FileNotFound
    //val inputFile= new File("src/main/resources/medium.csv")
    val fileSize = inputFile.length
    val testInput= FileIO.fromFile(inputFile)

    /**
      * My RunnableGraph is:
      *  Source     ~>            Flow             ~>           Sink
      * testInput      Tcp().OutgoingConnection()         totalTransferredBytes
      *                                               where totalTransferredBytes += sizeof(x)
      *                                               with x as the incoming ByteStreams
      */

    val result = testInput.via(Tcp().outgoingConnection(address, port)).runFold(ByteString.empty) { (acc, in) ⇒ acc ++ in }

    result.onComplete{
      case Success(result) =>
        println(s"Transferred Bytes: ${result.utf8String} of $fileSize")
        if(fileSize==(result.utf8String).toInt)
          transferClientRef ! SuccessfulTransmission
        else transferClientRef ! FailedTransmission
        println("Shutting down TransferClient")
        system.terminate()
      case Failure(e) =>
        println(s"Error in client: ${e.getMessage}")
        transferClientRef ! FailedTransmission
        system.terminate()
    }
  }

}

class TransferClient(backendNodeRef: ActorRef) extends Actor{
  import context._
  import context.dispatcher
  import ClusterMessage._

  /**
    * This method tries to create a Akka Stream System
    * */
  val systemClient= ActorSystem("SendingClient")
  val transferClientRef= self
  //val serverAddress = "127.0.0.1"
  //val serverPort = 6002
  //TransferClient.client(systemClient,serverAddress,serverPort,transferClientRef,filePath)

  def receive = {
    case msg: AskTransmission =>
      println(s" The Transfer client ${self} is creating its client to ${msg.address}:${msg.port}")
      TransferClient.client(systemClient,msg.address, msg.port,transferClientRef,msg.filePath)
    case FileNotFound =>
      backendNodeRef ! FailedTransmission
    case SuccessfulTransmission =>
      backendNodeRef ! SuccessfulTransmission
    case FailedTransmission =>
      backendNodeRef ! FailedTransmission
  }


}
