package thesis.remote.project.server

import akka.actor._
import sys.process._
import thesis.remote.project.common.Messages._
import thesis.remote.project.server.ClusterMessage._
import java.io.File

object GathererActor{
  def props(tmpFilePath: String, pathLib: String) : Props = Props(classOf[GathererActor],tmpFilePath, pathLib)

  def retrieveAbsoluteJARFilePath(filepath: String) : Option[String] = {
    val checkIfJarPresent= new File(filepath)
    if(checkIfJarPresent.exists()){
      Some(checkIfJarPresent.getAbsolutePath)
    }
    else
      None
  }

  def retrieveAbsoluteFilePath(filePath: String): Option[String] = {
    val checkfilePath = new File(filePath)
    if (checkfilePath.exists()) {
      Some(checkfilePath.getAbsolutePath)
    }
    else
      None
  }

}

class GathererActor(tmpFilePath: String, pathLib: String) extends Actor{
  import GathererActor._
  val numberOfClasses= 3
  val tempDir = new File(tmpFilePath)
  val tempFile = File.createTempFile("collected_result_",".txt", tempDir)
  val tempFilePath: String = tempFile.getAbsolutePath


  def receive = {
    case msg:AskResult=> {
      val order: String = if (msg.ascendingOrder) "ascending" else "descending"
      val stringJarPath = pathLib+"collect.jar"
      val absoluteJARPath = retrieveAbsoluteJARFilePath(stringJarPath)
      val resultPath = retrieveAbsoluteFilePath(msg.inputPath)
      if (absoluteJARPath.isDefined && resultPath.isDefined) {
        println(s"JAR PATH is: ${absoluteJARPath.get} and FILEPATH is: ${resultPath.get}")
        /** There are two possible ways of specifying the java -jar execution
          * 1 ) val execution= Seq("/bin/sh","-c",s"java -jar ${absoluteJARPath.get} -k ${msg.k} -in ${msg.inputPath} -order ${order}").!
          * Because we ask for a shell and then, by putting the "-c" flag, commands are read from string.
          * 2) the below execution where if we have a <-flag value> item, we insert in the Seq first the -flag and then the value as
          * two separate strings!
          * */
        val execution = Seq("java", "-jar", s"${absoluteJARPath.get}",
          "-k", s"${msg.k}",
          "-in", s"${resultPath.get}",
          "-out", s"${tempFilePath}",
          "-order", s"${order}").!
        val resultFile= new File(tempFilePath)
        if(resultFile.exists()){
          import scala.io.Source
          val bufferedSource = Source.fromFile(resultFile)
          var classLabel: Option[String] = None
          for (line <- bufferedSource.getLines) {
            println(line)
            classLabel= Some(line)
          }
          bufferedSource.close
          if(classLabel.isDefined)
            sender() ! ClassificationResult(classLabel.get)
          else
            sender() ! FailedJob
        }
      }
      else {
        println("No path defined for JAR")
        sender() ! FailedJob
      }
    }
  }

}
