package thesis.remote.project.util

import java.io.File
import java.nio.file.{Files, Path, Paths}

object Utils {
def getListOfFiles(dir: String):List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).toList
    } else {
      List[File]()
    }
  }

  def removeDirAndFiles(dirPath:String) ={
  val filesToBeDeleted= getListOfFiles(dirPath)
    if(!filesToBeDeleted.isEmpty)
      filesToBeDeleted.foreach( f => f.delete())
    new File(dirPath).delete()
  }
}
