package thesis.remote.project.server


import akka.actor.ActorSystem
import akka.actor.{PoisonPill, Props, ActorSelection, Address, AddressFromURIString}
import akka.cluster.Cluster
import akka.cluster.singleton.{ClusterSingletonManager, ClusterSingletonManagerSettings}
import com.typesafe.config.ConfigFactory
import akka.cluster.client.ClusterClientReceptionist
import akka.actor.ActorSystemImpl
import scala.collection.immutable


object MainApp extends App {

  //TODO: READ FROM ARGS: joinseed for other server in another pc, home folder and where we can find SUSY/Thyroid datasets
  override def main(args: Array[String]): Unit = {
    val usage =
      """
    Usage: [-seed addressOfClusterSeed] [-home homeFolder] [-setSusy path_to_folder] [-setThyroid path_to_folder] \n
        |e.g. -seed "akka.tcp://CollaborativeCluster@192.168.0.158:2552"
      """
    //if (args.length == 0) println(usage)
    val arglist = args.toList
    type OptionMap = Map[Symbol, Any]

    def nextOption(map: OptionMap, list: List[String]): OptionMap = {
      def isSwitch(s: String) = (s(0) == '-')
      list match {
        case Nil => map
        case "-seed" :: value :: tail =>
          nextOption(map ++ Map('seed -> value), tail)
        case "-home" :: value :: tail =>
          nextOption(map ++ Map('home -> value), tail)
        case "-setSusy" :: value :: tail =>
          nextOption(map ++ Map('setSusy -> value), tail)
        case "-setThyroid" :: value :: tail =>
          nextOption(map ++ Map('setThyroid -> value), tail)
        case option :: tail => println("Unknown option " + option); Map()
      }
    }

    val options = nextOption(Map(), arglist)
    //println(options)

    // OPTIONAL

    val seedfromArgs: Option[String] = options.get('seed) match {
      case Some(s: String) => Some(s)
      case _ => None //If I do not add the seed is that because either I have them or I am the cluster seed
    }

    // COMPULSORY ARGS

    val pathSUSYfromArgs: Option[String] = options.get('setSusy) match {
      case Some(s: String) => Some(s)
      case _ => None //If I do not add the path from which I can take the files..stop
    }

    val pathTHYROIDfromArgs: Option[String] = options.get('setThyroid) match {
      case Some(s: String) => Some(s)
      case _ => None //If I do not add the path from which I can take the files..stop
    }

    val homefromArgs: Option[String] = options.get('home) match {
      case Some(s: String) => Some(s)
      case _ => None //If I do not add the homepath from which I can take the lib..stop
    }

    if (homefromArgs.isEmpty || pathSUSYfromArgs.isEmpty || pathTHYROIDfromArgs.isEmpty) {
      println(usage)
      System.exit(1)
    }

    else {
      val systemName = "CollaborativeCluster"
      val system1 = ActorSystem(systemName, ConfigFactory.load("clusternode"))

      //Add our system to the cluster: if there is already one up elsewhere connect to it through the seed
      if(seedfromArgs.isDefined){
        //val joinseedAddress = AddressFromURIString("akka.tcp://CollaborativeCluster@192.168.0.158:2552")
        val joinseedAddress = AddressFromURIString(seedfromArgs.get)
        val myseq : immutable.IndexedSeq[Address] = Vector(joinseedAddress)
        Cluster(system1).joinSeedNodes(myseq)
      }
      else {
        //I must start myself the cluster
        val joinAddress = Cluster(system1).selfAddress
        Cluster(system1).join(joinAddress)
      }

      //Create a Listener to log (onscreen) all the changes to the cluster (add, removal...)
      system1.actorOf(Props[MemberWatcher], "memberListener")

      //Create a master passing also the paths that will define it
      val master = system1.actorOf(Props(classOf[ClusterNode], "master", homefromArgs.get, pathSUSYfromArgs.get, pathTHYROIDfromArgs.get), "master")
      //I want to have a Receptionist linked to this actorRef "master"
      ClusterClientReceptionist(system1).registerService(master)

      /*
        Thread.sleep(5000)
        val system2 = ActorSystem(systemName, ConfigFactory.load("frontend"))
        Cluster(system2).join(joinAddress)
        system2.actorOf(Props(classOf[ClusterNode],"node2"), "node2")

      Thread.sleep(1000)
      val system3 = ActorSystem(systemName, ConfigFactory.load("master"))
      Cluster(system3).join(joinAddress)
      val masterNOde = system3.actorOf(Props(classOf[ClusterNode], "node2"), "node2")
      //system3.actorOf(Props[RequestActor], "Kathy")
      //ClusterClientReceptionist(system3).registerService(masterNOde)


      system3.actorOf(
        ClusterSingletonManager.props(
          ClusterNode.props("master"),
          PoisonPill,
          ClusterSingletonManagerSettings(system3)
        ),
        "master")
    */
    }
  }
}
