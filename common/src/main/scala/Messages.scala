package thesis.remote.project.common


/**
  * Here we put all the possible messages that can be sent inside the cluster
  */

import akka.actor.ActorRef

object Messages {

  /** Messages should be serializable or during their network transmission strange things will happen. Remember Big, Little Endian? */
  //@SerialVersionUID(1L)
  //sealed trait OPmsg extends Serializable

  //___________Simple Requests as Objects____________

  case object askShutdown

  case object askStartup

  case object readyForTask

  case object failedTask

  case object positiveAck

  case object nACK

  //___________Complex Requests as Case Classes_________
  trait clientClusterCommunication

  final case class successfulTaskResult(label: String, majordomo: ActorRef) extends clientClusterCommunication

  final case class ExternalRequest(from: ActorRef, mLRequest: MLRequest) extends clientClusterCommunication

  final case class ClusterRequest(majordomoNode: ActorRef, clusterNode: ActorRef, mLRequest: MLRequest) extends clientClusterCommunication

  //final case class Request2Backend(majordomoNode: ActorRef, mLRequest: MLRequest)

  final case class MLRequest(algorithm: String, similarity: String, k: Int, item:String) extends clientClusterCommunication

  final case class majordomoTask(majordomoNode: ActorRef, clusterNode: ActorRef, res: Boolean) extends clientClusterCommunication

}

/*
trait OPmsg
  //___________Simple Requests as Objects____________//
  case object askShutdown extends OPmsg

  case object askStartup extends OPmsg

  case object readyForTask extends OPmsg

  case object successfulTask extends OPmsg

  case object failedTask extends OPmsg

  //___________Complex Requests as Case Classes_________//
  final case class ExternalRequest(from: ActorRef, mLRequest: MLRequest) extends OPmsg

  final case class ClusterRequest(majordomoNode: ActorRef, clusterNode: ActorRef, mLRequest: MLRequest) extends OPmsg

  final case class Request2Backend(majordomoNode: ActorRef, mLRequest: MLRequest) extends OPmsg

  final case class MLRequest(algorithm: String, similarity: String, k: Int) extends OPmsg

  final case class majordomoTask(majordomoNode: ActorRef, clusterNode: ActorRef, obj: OPmsg) extends OPmsg
  */