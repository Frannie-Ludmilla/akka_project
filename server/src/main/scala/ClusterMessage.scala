package thesis.remote.project.server

object ClusterMessage {
  trait Error
  case object NoFrameworkDetected extends Error
  case object FailedJob extends Error
  case object FailedTransmission extends Error
  case object Timeout extends Error
  case object FileNotFound extends Error
  case object CannotStartTransmissionServer extends Error

  trait PositiveAck
  case object FrameworkFound extends PositiveAck
  final case class JobFinalized(filePath: String) extends PositiveAck
  case object SuccessfulTransmission extends PositiveAck
  case object AllDone extends PositiveAck
  final case class SuccessfulBinding(address: String,port: Int) extends PositiveAck
  final case class ClassificationResult(label: String) extends PositiveAck

  trait BNEvents
  case object NodeShutdown extends BNEvents


  trait Commands
  case object AskFramework extends Commands
  case object AskJob extends Commands
  final case class AskTransmission(address: String, port:Int,filePath:String) extends Commands
  final case class AskResult(k: Int, inputPath: String, ascendingOrder: Boolean) extends Commands
  case object CancelJob extends Commands
  case object ShutdownServerFT extends Commands
  case object AskWorkerShutdown extends Commands
}

object FSMState{
  sealed trait FSMState
  case object Start extends FSMState
  case object Setup extends FSMState
  case object PerformTask extends FSMState
  case object Transmission extends FSMState
  case object ShutdownState extends FSMState
}

object ADT{
  case class NecessaryPaths(home: String, pathSusy: String, pathThyroid: String)
}
