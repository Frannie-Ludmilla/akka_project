//Create a package for the app
package thesis.remote.project

import scala.concurrent.duration._
import com.typesafe.config.ConfigFactory
import akka.actor.{ActorSystem, Props}

object FrontendApplication{

  def main(args: Array[String]): Unit = {
    if (args.isEmpty || args.head == "Frontend") {
      println("Sys: Executing main")
      startRemoteFrontendSystem()
    }
    //if (args.isEmpty || args.head == "Lookup") startRemoteLookupSystem()
  }

  def startRemoteFrontendSystem(): Unit = {
    val system = ActorSystem("FrontendSystem",
      ConfigFactory.load("frontend"))
    println("Sys: Started FrontendSystem - waiting for messages")
    val actor= system.actorOf(Props[FrontendActor], "frontend")
    actor ! Greet("Ludmilla")
    actor ! Greet("Frannie")
    actor ! Request
    actor ! MLRequest("knn", "cosine", 5)
    actor ! Greet("Frannie")
    actor ! ShutdownRequest
    //actor ! "Nice to meet you"

    Thread.sleep(1000)
    println("Sys: Shutdown")
    //watch actor and then terminate
    system.terminate()
  }


  def testNoNetwork(fileLength: Int, chunkSize: Int) {
    implicit val actorSystem = ActorSystem("test", ConfigFactory.parseString( """akka {
                                                                                |  log-dead-letters = 0
                                                                                |  log-dead-letters-during-shutdown = off
                                                                                |}""".stripMargin))
    val settings = MaterializerSettings()
    val materializer = FlowMaterializer(settings)

    def bytes = Iterator.fill(fileLength)(1.toByte)

    val testComplete = Flow(bytes).grouped(chunkSize).map(
      bytesIt => {
        val b = new ByteStringBuilder()
        b.sizeHint(chunkSize)
        b ++= bytesIt
        b.result()
      }
    ).fold(0)({ _ + _.length }).map({
      case completed @ `fileLength` =>
        s"---- transfer OK: $fileLength/$completed, chunkSize: $chunkSize"
      case completed =>
        throw new RuntimeException(s"---- transfer KO. expected: $fileLength got: $completed diff: ${fileLength - completed}")
    }).toFuture(materializer).andThen({ case _ =>
      actorSystem.shutdown()
      actorSystem.awaitTermination(3.seconds)
    })(ExecutionContext.global)

    println(Await.result(testComplete, atMost = 30.second))
  }

}