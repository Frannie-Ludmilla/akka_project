package thesis.remote.project.server

import java.io.File
import java.nio.file.{Files, Path, Paths}

import akka.actor.{Actor, ActorPath, ActorRef, ActorSelection, ActorSystem, Cancellable, ExtendedActorSystem, Extension, ExtensionKey, Identify, PoisonPill, Props}
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.{Publish, Send, SendToAll, Subscribe, Unsubscribe, UnsubscribeAck}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.cluster.client.{ClusterClient, ClusterClientReceptionist, ClusterClientSettings}
import thesis.remote.project.common.Messages._
import thesis.remote.project.server.ClusterMessage.{ShutdownServerFT, _}

import scala.concurrent.duration._
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Sink, Source, Tcp}
import akka.util.ByteString

import scala.util.{Failure, Success}
import thesis.remote.project.util.Utils
import akka.cluster.{Cluster, ClusterActorRefProvider}

object MajordomoActor{
  def props(name: String, clientToAttend:ActorRef, pathLib: String) : Props = Props(classOf[MajordomoActor], name, clientToAttend, pathLib)
}

class MajordomoActor(name : String, clientToAttend: ActorRef, pathLib: String) extends Actor{
  import context._
  import context.dispatcher


  def scheduler = context.system.scheduler

  val remoteHelpers= scala.collection.mutable.Set[ActorRef]()
  val helpersJobDone= scala.collection.mutable.Set[ActorRef]()
  val pathTmp=   Paths.get(".")
  val resultsPath= Files.createTempDirectory(pathTmp,"tmpMajordomo").toString
  var serverPort: Option[Int] = None

  /*
  class MyExtensionImpl(system: ExtendedActorSystem) extends Extension {
    def address = system.provider match {
      case rarp: ClusterActorRefProvider => rarp.transport.address
      case _ => system.provider.rootPath.address
    }
  }

  object MyExtension extends ExtensionKey[MyExtensionImpl]

  val address = MyExtension(system).address*/
  //val serverAkka= self.path.address.host
  val serverAkka= Cluster(context.system).selfAddress.host
  var serverAddress= if(serverAkka.isDefined) serverAkka.get else "127.0.0.1"
  val serverActorRef = context.system.actorOf(Props(classOf[TransferServer], self, serverAddress, 6001, resultsPath),"serverTransfer")
  var k= 0
  var orderAscending: Option[Boolean]= None
  var labelClass= ""
  val gatherer= context.actorOf(Props(classOf[GathererActor], resultsPath, pathLib), "Gatherer")
  var cancellable : Cancellable = null

  /**PreStart tells what the ActorSystem must do before firing it up the Actor*/
  override def preStart(): Unit ={
    scheduler.scheduleOnce(30.minutes, self, Timeout)
    //TODO: Create here server for incoming file transfer
    // actorRef, Address, Port, reults path
    //server(context.system, "127.0.0.1",6000)

    context.watch(serverActorRef)
  }


  // override postRestart so we don't call preStart and schedule a new Tick
  override def postRestart(reason: Throwable): Unit = ()

  def receivedData(): Boolean = {
    if(k!=0 && !orderAscending.isEmpty) true else false
  }

  def collectTasks : Receive = {
    case msg: MLRequest =>
      k= msg.k
      //Why? If I add more similarities I must say whether the best results are the smallest
      //orderAscending=true or the biggest (e.g. cosine) orderAscending=false
      orderAscending= msg.similarity match {
        case s if s matches "(?i)euclidean" => Some(true)
        case s if s matches "(?i)gower" => Some(true)
        case _ => Some(true)
      }
    case NoFrameworkDetected =>
      println(s"This Node, ${sender()}, cannot perform any task")
    case FailedJob =>
      if (remoteHelpers contains sender())
      remoteHelpers -= sender()
    //If the backend node in a cluster node has any framework, it can help me
    case FrameworkFound =>
      remoteHelpers += sender()
      //If I have already created an actor for TCP transfer I can give to the remote Helper the address and port
      if(serverPort.isDefined)
        sender() ! SuccessfulBinding(serverAddress, serverPort.get)
    case AllDone =>
      helpersJobDone+= sender()
      if(helpersJobDone.size == remoteHelpers.size){
        println("Everyone has finished")
        //clientToAttend ! successfulTask
        context.unwatch(serverActorRef)
        serverActorRef ! ShutdownServerFT
        //self ! PoisonPill
        if(receivedData()){
          println(resultsPath)
          gatherer ! AskResult(k,resultsPath,orderAscending.get)
          become(processingGatheredData)
        }
        else{// If I am here everyone has finished but I do not have all the data to forward to the GathererActor
          self ! CancelJob
          become(shuttingDown)
        }
      }
    /**If the Timeout has expired, we will start looking into the ones we have*/
    case Timeout =>
      val jobsNotFinished= remoteHelpers.diff(helpersJobDone)
      jobsNotFinished.foreach{
        actor =>
          actor ! CancelJob
      }
      /** If there are no finished tasks go to shutting down state, else process them*/
      if(helpersJobDone.size !=0 && receivedData()) {
        context.unwatch(serverActorRef)
        serverActorRef ! ShutdownServerFT
        //self ! PoisonPill
        gatherer ! AskResult(k, resultsPath, orderAscending.get)
        become(processingGatheredData)
      }
      else
        become(shuttingDown)
    case msg:SuccessfulBinding =>
      println(f"server address is ${serverAddress}")
      serverPort= Some(msg.port)
      remoteHelpers.foreach(actor =>
        actor ! SuccessfulBinding(msg.address, msg.port)
      )
    case CannotStartTransmissionServer =>
      //Close everything as I cannot receive the data
      remoteHelpers.foreach(actor =>
        actor ! CancelJob
      )
  }

  def processingGatheredData : Receive = {
    case FailedJob =>
      clientToAttend ! `failedTask`
      gatherer ! PoisonPill
      Utils.removeDirAndFiles(resultsPath)
      context stop(self)
    case msg: ClassificationResult =>
      labelClass= msg.label
      clientToAttend ! successfulTaskResult(labelClass, self)
      //cancellable= scheduler.scheduleOnce(1.minutes, self, Timeout)
      context.unwatch(gatherer)
      gatherer ! PoisonPill
      Utils.removeDirAndFiles(resultsPath)
    case `positiveAck` =>
      //if(cancellable!=null)
       // cancellable.cancel()
      context stop(self)
    case Timeout =>
      clientToAttend ! successfulTaskResult(labelClass,self)
    case _=> println(s"Unknown type of msg from ${sender()}")
  }

  //ensure that both the gatherer and server are shutting down
  def shuttingDown : Receive = {
    case _=>
      Utils.removeDirAndFiles(resultsPath)
      context.unwatch(gatherer)
      gatherer ! PoisonPill
      context.unwatch(serverActorRef)
      serverActorRef ! ShutdownServerFT
      println(sender())
      context stop(self)
  }

  def receive = collectTasks

}
