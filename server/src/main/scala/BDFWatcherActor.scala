package thesis.remote.project.server


import akka.actor._
import thesis.remote.project.common.Messages._
import thesis.remote.project.server.ClusterMessage._
import java.nio.file.{Files, Paths}
import java.io.File
import thesis.remote.project.server.ADT.NecessaryPaths
//import thesis.remote.project.server.{FlinkUtils, SparkUtils}
import thesis.remote.project.util.Utils

object BDFWatcherActor {
  def props(name:String, paths: NecessaryPaths) : Props = Props(classOf[ClusterNode], name, paths)
}


class BDFWatcherActor(name:String, paths: NecessaryPaths) extends Actor {

  import context._
  import BDFWatcherActor._

  object Framework extends Enumeration {
    type Framework = Value
    val FLINK, SPARK, NONE = Value
  }

  //val pathTmp= Paths.get("./target/")
  val outputFilePath = Files.createTempDirectory("tmpBDFWatcher_").toAbsolutePath.toString
  //"./src/main/resources/medium.csv"
  var selectedFramework = Framework.NONE
  var receivedMLTask: Option[MLRequest] = None


  def Start: Receive = {
    case msg: MLRequest => {
      receivedMLTask = Some(msg)
      println(s"${self} am alive, a BDF watcher, I received AskFramework")
      val flinkPresent: Boolean = FlinkUtils.checkFlink()
      val sparkPresent: Boolean = SparkUtils.checkSpark()
      if (flinkPresent && FlinkUtils.checkAvailability(receivedMLTask.get.algorithm, paths)) {
        println("Flink is present")
        sender() ! FrameworkFound
        selectedFramework = Framework.FLINK
        become(PerformTask)
      }
      else if (sparkPresent && SparkUtils.checkAvailability(receivedMLTask.get.algorithm,paths)) {
        println("Spark is present")
        sender() ! FrameworkFound
        selectedFramework = Framework.SPARK
        become(PerformTask)
      }
      else {
        println("I should not be hereeee: Danger ZONE because no Framework detected")
        selectedFramework = Framework.NONE
        //If we have arrived here we have no framework
        sender() ! NoFrameworkDetected
      }
    }
  }

  def PerformTask: Receive = {
    case AskJob =>
      if (receivedMLTask.isEmpty || selectedFramework==Framework.NONE) sender() ! FailedJob
      else {
        val task =
          if (selectedFramework == Framework.SPARK)
            SparkUtils.launchingProgram(receivedMLTask.get.algorithm, receivedMLTask.get.k,
              receivedMLTask.get.similarity, outputFilePath, receivedMLTask.get.item, paths)
          else
            FlinkUtils.launchingProgram(receivedMLTask.get.algorithm, receivedMLTask.get.k,
              receivedMLTask.get.similarity, outputFilePath, receivedMLTask.get.item, paths)
        println(s"${self} BDF watcher am sending a JobFinalized if ${task == true}")
        if (!task) //task should return true for successful completion
          sender() ! FailedJob
        else
          sender() ! JobFinalized(outputFilePath)
      }
    case AskWorkerShutdown =>
      context stop(self)
  }

  def receive = Start
}
