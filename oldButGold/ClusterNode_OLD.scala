package thesis.remote.project

import akka.actor._
import akka.serialization._
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.{Publish, Subscribe, Unsubscribe, UnsubscribeAck}
import akka.cluster.client.ClusterClientReceptionist
import akka.cluster.Cluster
import akka.persistence.PersistentActor


/** Factory Methods for ClusterNode instances. */
object ClusterNode{
  def props(name:String) : Props = Props(classOf[ClusterNode], name)


  /** Definitions of the types of messages I can either send or receive*/
  case class Message(from: ActorRef, text: String)
  case class Publish(msg: String)
}


/** ClusterNode is the actor that acts as a Cluster Node, with a mediator to publish.
  * We will send messages from RequestActor to ClusterNode such that this last one can
  * then spread the Request for the ML task to all the other nodes in the Cluster.
  *
  * @constructor creates an actor inside a DistributedPublisher-Subscriber pattern in akka
  * @param name the actor name
  */
class ClusterNode(name: String) extends Actor{
  //Need to import context or become() won't work
  import context._

  val cluster = Cluster(context.system)

  val mediator = DistributedPubSub(context.system).mediator

  /**I have to specify the topic*/
  val topic = "MLTask"
  mediator ! Subscribe(topic,self)
  println(s"Actor $self joined the cluster", self)


  def busyWithReq(inquirer: ActorRef, backend: ActorRef) : Receive = {
    case `readyForTask` =>
      backend ! MLRequest("KNN","cosine",10)
    case `failedTask` =>
      //I reply to the node who sent the request inside the cluster
      inquirer ! `failedTask`
      //I shut down my backend
      backend ! PoisonPill
      println(s"$self - Switching from state: busyWithReq to: Available ")
      become(available)
    case `successfulTask` =>
      println(s"$self Sending successfulTask to $inquirer")
      inquirer ! successfulTask
      println(s"$self - Switching from state: busyWithReq to: Available ")
      become(available)
      //println("Work Done. Now I can perform suicide")
      //cluster.leave(self.path.address)
  }



  def available : Receive= {
    case ClusterNode.Message(from, text) =>
      val direction = if (sender == self) ">>>>" else s"<< $from:"
      println(s"$self $direction $text")
      if(sender!=self){
        println(s"message whose sender is $sender, from $from while I am $self")
        println(s"$self - Switching from state: Available to: busyWithReq")
        println(s"$self - Creating new BackendNode")
        val backendNode: ActorRef= system.actorOf(Props[BackendNode])
        backendNode ! `askStartup`
        become(busyWithReq(from, backendNode))
      }
      /** I have received a msg that asks me to broadcast the request to all = I publish sth to all the subscribers*/
    case ClusterNode.Publish(msg) =>
      mediator ! Publish(topic, ClusterNode.Message(sender,msg))
    case `askShutdown` =>
      /**Unsubscribe from topic when asked to shutdown*/
      mediator ! Unsubscribe(topic,self)
      /** When I receive the ack for my unsubscription I can shut myself down*/
    case UnsubscribeAck =>
      context.stop(self)
    case x: String =>
      println(s"I received this: $x from ${sender()}")
      val majordomo= system.actorOf(Props(classOf[MajordomoActor], sender()),"majordomo")
      mediator ! Publish(topic, ClusterNode.Message(sender(), x))
  }

  def receive = available
}

val allButtons: List[Button] = List(new Button("red",false, "red"),new Button("green",false, "red"))
//def isRedButton(string: String) : Boolean = true

object TrainHowTo extends App {

  class Button(name: String, var active: Boolean, color: String) {
    def push(): Unit = {active = !active}
    def isRedButton(): Boolean = color.equalsIgnoreCase("red")
  }

  val allButtons: List[Button] = List(new Button("red",false, "red"),new Button("green",false, "red"))

  val DeadMan_switch = new Button("KillSwitch", false, "black")

  def getThisTrainRunning: Unit = for {button <- allButtons if button isRedButton} button push

  def forever(): Unit = while (true) { DeadMan_switch push }
}