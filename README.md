##Project: A Collaborative Cluster for ML Tasks using Big Data frameworks.


This project build with **Akka** in **Scala** creates a `Publish-Subscriber Cluster` (_server_) such that a `RequestActor` (_client_) can send its requests with ML Tasks.

Only the nodes in the cluster that have subscribed to the topic _"MLTask"_ and are available at that moment will be notified when a request comes.

####Server side (_CollaborativeCluster_)

The Cluster act as a **Frontend** for the application (always up and running) because each node has a `ClusterClientReceptionist` that can be reached from actors/nodes that do not belong to the cluster (e.g. `RequestActor`).

Each time a request is received, the `ClusterNode` starts the  **Backend** by triggering the creation of a `MajordomoActor` that will take care of the request by gathering the intermediate results and sending them back to the related `RequestActor`.

A `BackendNode` **bn** will start either Apache Flink or Apache Spark according to the node configuration, will monitor the application and will send the sanitized outcome to the `MajordomoActor` that is in charge of that request.

####Client side

The `RequestActor` **req** needs to create a `ClusterClient `actor that will talk to the _CollaborativeCluster_ forwarding the messages via the `ClusterClientReceptionist` whose address **req** knows.

