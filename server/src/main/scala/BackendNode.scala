package thesis.remote.project.server

import java.io.File

import scala.concurrent.duration._
import akka.actor._
import akka.actor.FSM.{Event, _}
import thesis.remote.project.common.Messages._
import thesis.remote.project.server.ADT.NecessaryPaths
import thesis.remote.project.server.ClusterMessage._
import thesis.remote.project.server.FSMState._
import thesis.remote.project.util.Utils

//final case class requiredActRef(majordomo: ActorRef, parent: ActorRef, worker: ActorRef, attempts: Int)

object BackendNode{
  def props(majordomo: ActorRef, parent: ActorRef, paths: NecessaryPaths) : Props = Props(classOf[BackendNode], majordomo, parent, paths)
//TODO: ADD Cancel Job
}

class BackendNode(majordomo: ActorRef, parent: ActorRef, paths: NecessaryPaths) extends Actor {
  import context._

  val worker= context.actorOf(Props(classOf[BDFWatcherActor], "BDFWatcher", paths))
  val transferClientRef = context.actorOf(Props(classOf[TransferClient], self), "transferClient")
  var request: Option[MLRequest] = None
  var address:Option[String] = None
  var port : Option[Int] = None
  var filePath: Option[String] = None

  def retrieveAbsoluteHadoopFilePath(filePath: String): Option[String] = {
    val checkfilePath = new File(filePath)
    if (checkfilePath.isDirectory) {
      val hadoopFilePath = filePath + "/part-00000"
      val path = new File(hadoopFilePath)
      if (path.exists() && path.isFile) {
        Some(path.getAbsolutePath)
      }
      else
        None
    }
    else {
      if (checkfilePath.isFile) {
        Some(checkfilePath.getAbsolutePath)
      }
      else
        None
    }
  }


  override def preStart() : Unit = {
    context.watch(worker)
  }


  def Start : Receive = {
    case msg: MLRequest=>
      //worker ! AskFramework
      request= Some(msg)
      println(s"${self}, BN, is moving to Setup")
      worker ! msg
      become(Setup)
  }

  def Setup : Receive = {
    case FrameworkFound =>
      majordomo ! FrameworkFound
      worker ! AskJob
      worker ! request
      println(s"${self}, BN, is moving to PerformTask")
      become(PerformTask)
    case NoFrameworkDetected =>
      majordomo ! NoFrameworkDetected
      context.unwatch(worker)
      worker ! PoisonPill
      //self ! PoisonPill
      parent ! NodeShutdown
    case Terminated =>
      majordomo ! FailedJob
      parent ! NodeShutdown
    case msg:SuccessfulBinding=>
      address= Some(msg.address)
      port= Some(msg.port)
  }

  def PerformTask : Receive = {
    case msg:SuccessfulBinding=>
      address= Some(msg.address)
      port= Some(msg.port)
    case msg: JobFinalized =>
      //Go to transmission to try to transmit
      //only if I have the coordinates for File transfer
      filePath= Some(msg.filePath)
      //Check: if it is a directory it is an hadoop - type answer, take part-00000
      val absolutePath= retrieveAbsoluteHadoopFilePath(msg.filePath)
      if (address.isDefined && port.isDefined && absolutePath.isDefined) {
        transferClientRef ! AskTransmission(address.get,port.get,absolutePath.get)
        context.unwatch(worker)
        worker ! AskWorkerShutdown
        context.watch(transferClientRef)
        println(s"${self}, BN, is moving to Transmission")
        become(Transmission)
      } else{
        println("I do not have the details. I am shutting down")
        majordomo ! FailedJob //I cannot transmit my results
        }
    case FailedJob =>
      majordomo ! FailedJob
      worker ! PoisonPill
      parent ! NodeShutdown
    case Terminated =>
      majordomo ! FailedJob
      parent ! NodeShutdown
  }

  def Transmission : Receive = {
    case SuccessfulTransmission =>
      Utils.removeDirAndFiles(filePath.get)
      majordomo ! AllDone
      context.unwatch(transferClientRef)
      transferClientRef ! PoisonPill
      parent ! NodeShutdown
    case FailedTransmission =>
      majordomo ! FailedJob
      context.unwatch(transferClientRef)
      transferClientRef ! PoisonPill
      parent ! NodeShutdown
      //If I am here, Terminated could be sent only by the transferClient Actor
    case Terminated =>
      majordomo ! FailedJob
      parent ! NodeShutdown
  }

  def receive= Start

}


/*
with FSM[FSMState, requiredActRef] {

  import context._

  /** Starting point for my Finite State Machine */
  startWith(Start, null)

  when(Start) {
    case Event(msg: Request2Backend, _) =>
      val worker = system.actorOf(Props[BDFWatcherActor])
      context.watch(worker)
      val stateData = requiredActRef(msg.majordomoNode, sender(), worker,0)
      worker ! AskFramework
      goto(Setup) using stateData
  }
  //stateFunction argument is a PartialFunction[Event, State]

  when(Setup) {
    case Event(FrameworkFound, requiredActRef(majordomo, parent, worker,attempts)) =>
      //Send OK to majordomo, I can do it...prepare for the exchange later
      majordomo ! FrameworkFound
      //TODO: change for ML Request Later
      //Send OK to worker, keep on with the task
      worker ! successfulTask
      goto(PerformTask)
    case Event(NoFrameworkDetected, requiredActRef(majordomo, parent, worker,attempts)) =>
      //Send failed task to Majordomo: I cannot help you
      majordomo ! NoFrameworkDetected
      //Kill the worker...it is useless
      worker ! PoisonPill
      //Tell the ClusterNode to update its structures because I am shutting down
      parent ! askShutdown
      stop()
    case Event(Terminated(wn),requiredActRef(majordomo, parent, worker,attempts)) =>
      majordomo ! failedTask
      parent ! askShutdown
      stop()
    //case Event(Request2Backend, _) =>
    // stash()
  }

  when(PerformTask) {
    case Event(JobFinalized(filePath), requiredActRef(majordomo, parent, worker,attempts)) =>
      //Send Ok to majordomo, now the transmission has to take place
      //majordomo ! successfulTask
      //TODO: Kill this worker and create a client for transmission
      worker ! AskTransmission(filePath)
      goto(Transmission)
    case Event(FailedJob, requiredActRef(majordomo, parent, worker,attempts)) =>
      //majordomo ! failedTask
      //Kill the worker...it is useless
      worker ! PoisonPill
      //Tell the ClusterNode to update its structures because I am shutting down
      parent ! askShutdown
      stop()
    case Event(Terminated(wn),requiredActRef(majordomo, parent, worker,attempts)) =>
      majordomo ! failedTask
      parent ! askShutdown
      stop()
  }

  when(Transmission) {
    case Event(SuccessfulTransmission, requiredActRef(majordomo, parent, worker,attempts)) =>
      //Tell successful
      majordomo ! majordomoTask(majordomo, context.parent, true)
      stop()
      //Try to see if later try to
    /*case Event(FailedTransmission, requiredActRef(majordomo, parent, worker,attempts)) =>
      //try again
      val newStateData= requiredActRef(majordomo,parent,worker,attempts+1)
      stay using (newStateData)*/
    case Event(FailedTransmission, requiredActRef(majordomo, parent, worker, attempts))=>
      majordomo ! FailedJob
      worker ! PoisonPill
      //Tell the ClusterNode to update its structures because I am shutting down
      parent ! askShutdown
      stop()
    case Event(Terminated(wn),requiredActRef(majordomo, parent, worker,attempts)) =>
      majordomo ! FailedJob
      parent ! askShutdown
      stop()
  }

}*/


